﻿using AutoMapper;
using MeuProjeto.Domain.Models;
using MeuProjeto.WebApi.Dtos;

namespace MeuProjeto.WebApi
{
    public class WebApiMapperProfile : Profile
    {
        public WebApiMapperProfile()
        {
            CreateMap<Filme, FilmesGetResult>();
            CreateMap<FilmesGet, Pesquisa>();
        }
    }
}
